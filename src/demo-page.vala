[GtkTemplate (ui = "/org/example/App/ui/demo-page.ui")]
public class TabsWidget.DemoPage : Gtk.Bin {
    [GtkChild]
    private Gtk.Entry title_entry;
    [GtkChild]
    private Gtk.Entry tooltip_entry;
    [GtkChild]
    private Gtk.Switch pinned_switch;
    [GtkChild]
    private Gtk.Switch needs_attention_switch;

    [GtkChild]
    private Gtk.Switch loading_switch;
    [GtkChild]
    private Gtk.Switch has_icon_switch;
    [GtkChild]
    private Gtk.Entry icon_entry;

    [GtkChild]
    private Gtk.Switch has_secondary_icon_switch;
    [GtkChild]
    private Gtk.Entry secondary_icon_entry;

    public bool has_icon { get; set; }
    public bool has_secondary_icon { get; set; }
    public bool secondary_icon_activatable { get; set; }
    public bool confirm_close { get; set; }

    public Icon? icon { get; set; }
    public Icon? secondary_icon { get; set; }

    private weak Hdy.TabPage page;
    private Hdy.TabView view;

    [GtkCallback]
    private void update_icon () {
        if (has_icon_switch.active) {
            try {
                icon = Icon.new_for_string (icon_entry.text);
            } catch (Error e) {
                critical ("Cannot parse icon: %s", e.message);
            }

        } else {
            icon = null;
        }
    }

    [GtkCallback]
    private void update_secondary_icon () {
        if (has_secondary_icon_switch.active) {
            try {
                secondary_icon = Icon.new_for_string (secondary_icon_entry.text);
            } catch (Error e) {
                critical ("Cannot parse icon: %s", e.message);
            }

        } else {
            secondary_icon = null;
        }
    }

    [GtkCallback]
    private void close_cb () {
        view.close_page (page);
    }

    private bool string_null_to_empty (Binding binding, Value input, ref Value output) {
        var input_str = input.get_string ();

        if (input_str == null)
            input_str = "";

        output.set_string (input_str);

        return true;
    }

    private bool string_empty_to_null (Binding binding, Value input, ref Value output) {
        var input_str = input.get_string ();

        if (input_str == "")
            input_str = null;

        output.set_string (input_str);

        return true;
    }

    public bool try_close () {
        if (!confirm_close)
            return Gdk.EVENT_PROPAGATE;

        var dialog = new Gtk.MessageDialog (
            get_toplevel () as Gtk.Window,
            Gtk.DialogFlags.MODAL,
            Gtk.MessageType.WARNING,
            Gtk.ButtonsType.CANCEL,
            "%s",
            "Do you want to close this tab?"
        );
        dialog.format_secondary_text ("%s", "A form you modified has not been submitted.");

        var button = dialog.add_button ("_Discard form", Gtk.ResponseType.ACCEPT);
        button.get_style_context ().add_class ("destructive-action");

        dialog.set_default_response (Gtk.ResponseType.CANCEL);

        dialog.response.connect (response => {
            dialog.destroy ();

            view.close_page_finish (page, response == Gtk.ResponseType.ACCEPT);
        });

        dialog.present ();

        return Gdk.EVENT_STOP;
    }

    public void set_page (Hdy.TabView view, Hdy.TabPage page) {
        this.view = view;
        this.page = page;

        has_icon = page.icon != null;
        if (has_icon)
            icon_entry.text = page.icon.to_string ();

        has_secondary_icon = page.secondary_icon != null;
        if (has_secondary_icon)
            secondary_icon_entry.text = page.secondary_icon.to_string ();

        page.bind_property ("title", title_entry, "text", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        page.bind_property ("tooltip", tooltip_entry, "text", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL, string_null_to_empty, string_empty_to_null);
        page.bind_property ("icon", this, "icon", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        page.bind_property ("secondary-icon", this, "secondary-icon", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        page.bind_property ("secondary-icon-activatable", this, "secondary-icon-activatable", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        page.bind_property ("loading", loading_switch, "active", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        page.bind_property ("pinned", pinned_switch, "active", BindingFlags.SYNC_CREATE);
        page.bind_property ("needs-attention", needs_attention_switch, "active", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
    }
}
