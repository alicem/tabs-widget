public class TabsWidget.Application : Gtk.Application {
    private const ActionEntry[] action_entries = {
        { "window-new", window_new },
    };

    private Gtk.CssProvider css_provider;

    public Application () {
        Object (
            application_id: "org.example.App",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    construct {
        add_action_entries (action_entries, this);
    }

    private void load_theme (string theme) {
        css_provider.load_from_resource (@"/org/example/App/style/$theme.css");
    }

    private void check_theme () {
        var settings = Gtk.Settings.get_default ();

        var theme_name = settings.gtk_theme_name;
        var prefer_dark = settings.gtk_application_prefer_dark_theme;

        if (theme_name.has_suffix ("dark"))
            prefer_dark = true;

        if (theme_name.has_prefix ("io.elementary.stylesheet")) {
            load_theme (prefer_dark ? "elementary-dark" : "elementary");
            return;
        }

        css_provider.load_from_data ("");
    }

    protected override void startup () {
        base.startup ();

        Hdy.init ();

        // settings.gtk_application_prefer_dark_theme = true;

        css_provider = new Gtk.CssProvider ();
        Gtk.StyleContext.add_provider_for_screen (
            Gdk.Screen.get_default (),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );

        var settings = Gtk.Settings.get_default ();
        settings.notify["gtk-theme-name"].connect (() => {
            check_theme ();
        });
        settings.notify["gtk-application-prefer-dark-theme"].connect (() => {
            check_theme ();
        });
        check_theme ();
    }

    protected override void activate () {
		var win = active_window ?? new Window (this);

		win.present ();
    }

    private void window_new () {
        assert (active_window is Window);

        new Window.from (active_window as Window, true).present ();
    }
}
